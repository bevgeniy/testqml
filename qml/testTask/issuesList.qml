
import QtQuick 2.0
import QtQuick.Controls 1.1
import QtQuick.LocalStorage 2.0

ApplicationWindow  {
    id: issuesWnd
    width: 600
    height: 600
    property string user: ""
    property int role: 0
    property var listIssues:[]
    property var tUsrID: -2

    onUserChanged:
    {
        console.debug("Role: "+ role)
        console.debug("User: "+ user)
        console.debug("userID: "+ tUsrID)
        getIssues(tUsrID)
    }

    ListModel
    {
         id: modelIssues
    }

    ListView
    {
        id: listView1
        x: 59
        y: 75
        width: 385
        height: 383
        focus: true

        delegate: Item
        {
            width: parent.width
            height: 20

            Row
            {
                id: row1
                spacing: 10

                Text
                {
                    text: name
                    anchors.verticalCenter: parent.verticalCenter
                    font.bold: true
                }
            }

            MouseArea
            {
                id: itemMouseArea
                anchors.fill: parent
                onClicked:
                {
                    console.log("Item clicked:", index);
                    showIssue(listIssues[index])
                }
            }
        }

        model: modelIssues
    }

    function getIssues(userIDIn)
    {
        var db = LocalStorage.openDatabaseSync("DBIssues", "","DB",1000000);

        db.transaction( function(tx)
        {
            var rs;

            if(role == 0) //for users
            {
                rs = tx.executeSql('SELECT * from Issues where UserID=?', [userIDIn]);
                listIssues = []
                var index = 0

                for (var i = 0; i < rs.rows.length; i++)
                {
                    if(rs.rows.item(i).State !== 3)
                    {
                        listIssues[index] = rs.rows.item(i).ID
                        modelIssues.append({name: rs.rows.item(i).Name})
                        index ++
                    }
                }
            }
            else if(role == 1) //for admins
            {
                rs = tx.executeSql('SELECT * from Issues');
                listIssues = []

                for (var i = 0; i < rs.rows.length; i++)
                {
                    listIssues[i] = rs.rows.item(i).ID
                    modelIssues.append({name: rs.rows.item(i).Name})
                }
            }

        } )
    }

    function showIssue(ID)
    {
        var db = LocalStorage.openDatabaseSync("DBIssues", "","DB",1000000);

        db.transaction( function(tx)
        {
            var name = ""
            var description = ""
            var status = 0
            var rs = tx.executeSql('SELECT * from Issues where ID=?', [ID]);
            var issueUserID = 0
            var assignee = ""

            for (var i = 0; i < rs.rows.length; i++)
            {
                name = rs.rows.item(i).Name
                description = rs.rows.item(i).Description
                status = rs.rows.item(i).State
                issueUserID = rs.rows.item(i).UserID
            }

            if(role == 1)
            {
                rs = tx.executeSql('SELECT * from Login where ID=?', [issueUserID]);
                var temp = rs.rows.length

                for (var k = 0; k < rs.rows.length; k++)
                    assignee = rs.rows.item(k).Login
            }
            else
                assignee = user


            var component = Qt.createComponent("issue.qml")

            if( component.status != Component.Ready )
            {
                if( component.status == Component.Error )
                    console.debug("Error:"+ component.errorString());

                return; // or maybe throw
            }

            var statusStr = ""

            switch (status)
            {
                case 0:
                    statusStr = "New task"
                    break
                case 1:
                    statusStr = "In process"
                    break
                case 2:
                    statusStr = "Resolved"
                    break
                case 3:
                    statusStr = "Closed"
                    break
                default:
                    break
            }

            var window = component.createObject(issuesWnd, {issueID:ID, name: name, status: statusStr, description: description, assignee: assignee, role: role})
            window.show()
        } )
    }

    function addIssue()
    {
        var db = LocalStorage.openDatabaseSync("DBIssues", "","DB",1000000);

        db.transaction( function(tx)
        {
            var rs = tx.executeSql('INSERT into Issues (Name, State, UserID, Description) VALUES(\'NewName\', 0, ?, \'desription...\')', [tUsrID]);

        } )


        db.transaction( function(tx)
        {
            var rs = tx.executeSql('SELECT * from Issues order by ID desc limit 1');
            var id = 0

            for (var i = 0; i < rs.rows.length; i++)
            {
                id = rs.rows.item(i).ID
            }

            showIssue(id)

        } )
    }

    Text
    {
        id: text1
        x: 284
        y: 26
        text: "Issues"
        font.pixelSize: 12
    }

    Text
    {
        id: text2
        x: 284
        y: 46
        width: 32
        height: 14
        text: user
        font.pixelSize: 12
    }

    Button {
        id: btnAdd
        x: 59
        y: 478
        width: 93
        height: 23
        text: "Add"
        visible: role == 1 ? true :false

        onClicked:
        {
            console.log("Add issue");
            addIssue()
        }
    }
}
