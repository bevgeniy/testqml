import QtQuick 2.0
import QtQuick.Controls 1.1
import QtQuick.LocalStorage 2.0

ApplicationWindow{
    id: root
    width: 360
    height: 360
    property string user: ""

    TextInput {
        id: textInputLogin
        x: 41
        y: 61
        width: 184
        height: 20
        text: "Admin"
        z: 1
        font.pixelSize: 12
    }

    Rectangle {
        id: rectangleLogin
        x: 41
        y: 61
        width: 184
        height: 20
        color: "#ffffff"
        border.color: "#140000"
    }

    Rectangle {
        id: rectanglePass
        x: 41
        y: 111
        width: 184
        height: 20
        color: "#ffffff"
        border.color: "#140000"
    }

    TextInput {
        id: textInputPassword
        x: 41
        y: 111
        width: 184
        height: 20
        text: "Admin"
        font.pixelSize: 12
        z: 1
    }

    Text {
        id: textLogin
        x: 41
        y: 41
        width: 89
        height: 14
        text: "Login"
        font.pixelSize: 12
    }

    Text {
        id: textPassword
        x: 41
        y: 91
        width: 89
        height: 14
        text: "Password"
        font.pixelSize: 12
    }

    Button {
        x: 41
        y: 149
        text: "Login"
        onClicked:
        {
            logIn(textInputLogin.text, textInputPassword.text)
        }

    }

    function logIn(login, password)
    {
        var db = LocalStorage.openDatabaseSync("DBIssues", "","DB",1000000);

        db.transaction( function(tx)
        {
            var rs = tx.executeSql('SELECT * from Login where Login=? and Password=?', [login, password]);
            var user = ""
            var role = -1
            var userID = -1

            for (var i = 0; i < rs.rows.length; i++)
            {
                user = rs.rows.item(i).Login
                role = rs.rows.item(i).Role
                userID = rs.rows.item(i).ID
            }

            text1.textProperty = user

            if(text1.textProperty != "")
            {
                var component = Qt.createComponent("issuesList.qml")
                var window = component.createObject(root, {user: user, role: role, tUsrID: userID})
                window.show()
                root.user = user
                text1.textProperty = user
            }
            else
            {
                text1.textProperty = "Invalid login or password"
                //text1.color = "red"
            }
        } )
    }

    Text
    {
        property string textProperty: "?"
        id: text1
        x: 41
        y: 217
        width: 219
        height: 25
        font.pixelSize: 12
        text: textProperty
    }
}
