import QtQuick 2.0
import QtQuick.Controls 1.1
import QtQuick.LocalStorage 2.0

ApplicationWindow  {
    width: 600
    height: 600

    property int issueID: -1
    property string status: ""
    property string name: ""
    property string description: ""
    property string assignee: ""
    property int role: -1
    property var listUsers:[]

    TextEdit {
        id: descrItem
        x: 38
        y: 170
        width: 525
        height: 180
        text: description
        z: 1
        font.pixelSize: 12
    }

    Text {
        id: labelDescription
        x: 38
        y: 145
        width: 123
        height: 14
        text: "Description"
        font.pixelSize: 12
    }

    Rectangle {
        id: rectangle1
        x: 36
        y: 168
        width: 529
        height: 184
        color: "#ffffff"
        border.color: "#080000"
    }

    Text {
        id: issueName
        x: 38
        y: 72
        width: 42
        height: 14
        text: "Name : "
        horizontalAlignment: Text.AlignLeft
        font.pixelSize: 12

        TextEdit {
            id: nameTextEdit
            x: 47
            y: 0
            width: 388
            height: 14
            text: name
            font.pixelSize: 12
            readOnly: role == 1 ? false :true
        }
    }

    Text {
        id: issueid
        x: 38
        y: 52
        width: 123
        height: 14
        text: "ID : " + issueID
        font.pixelSize: 12
    }

    Text {
        id: issueStatus
        x: 38
        y: 92
        width: 58
        height: 14
        text: "Status : "
        font.pixelSize: 12
    }

    Text {
        id: text1
        x: 275
        y: 8
        width: 50
        height: 14
        text: "Issue"
        font.pixelSize: 12
    }

    Button {
        x: 36
        y: 394
        width: 100
        height: 23
        text: "Resolve"

        onClicked:
        {
            console.log("Set resolved issue " + issueID);
            resolveIssue()
        }
    }

    Text {
        id: issueAssignee
        x: 38
        y: 112
        width: 58
        height: 14
        text: role == 1 ? "Assignee : " : ("Assignee : " + assignee)
        font.pixelSize: 12
    }

    ListModel
    {
         id: modelCombo
    }

    ComboBox {
        id: assigneComboBox
        x: 102
        y: 112
        width: 125
        height: 16
        visible: role == 1 ? true :false
        model: modelCombo

        Component.onCompleted:
        {
            fillCombo()
        }
    }

    Button {
        x: 142
        y: 394
        width: 100
        height: 23
        text: "Save"
        checked: false
        visible: role == 1 ? true :false

        onClicked:
        {
            console.log("Save issue");
            saveIssue()
        }
    }

    ComboBox {
        id: statusComboBox
        x: 102
        y: 92
        width: 125
        height: 16
        enabled: role == 1 ? true :false
        model: ListModel {
               id: modelStatus
               ListElement { text: "New task"}
               ListElement { text: "In process"}
               ListElement { text: "Resolved"}
               ListElement { text: "Closed"}
           }
    }

    function saveIssue(ID)
    {
        var db = LocalStorage.openDatabaseSync("DBIssues", "","DB",1000000);

        var state = 0

       if(statusComboBox.currentText == "New task")
           state = 0
       else if(statusComboBox.currentText == "In process")
           state = 1
       else if(statusComboBox.currentText == "Resolved")
           state = 2
       else if(statusComboBox.currentText == "Closed")
           state = 3

        db.transaction( function(tx)
        {
            var rs = tx.executeSql('SELECT * from Login where Login=?', [assigneComboBox.currentText]);
            var id = 0

            for (var i = 0; i < rs.rows.length; i++)
            {
                assignee = rs.rows.item(i).ID
            }
        } )

        db.transaction( function(tx)
        {
            var rs = tx.executeSql('update Issues set Name=?, UserID=?, Description=?, State=? where ID=?', [nameTextEdit.text, assignee, descrItem.text, state, issueID])
        } )
    }

    function fillCombo()
    {
        var db = LocalStorage.openDatabaseSync("DBIssues", "","DB",1000000);

        db.transaction( function(tx)
        {
            var rs = tx.executeSql('SELECT * from Login');
            var id = 0

            for (var i = 0; i < rs.rows.length; i++)
            {
                  modelCombo.append({text: rs.rows.item(i).Login})
            }
        } )

        console.debug("assignee: " + assignee)
        var index = assigneComboBox.find(assignee)
        assigneComboBox.currentIndex = index
        var indexStatus = statusComboBox.find(status)
        statusComboBox.currentIndex = indexStatus
    }

    function resolveIssue()
    {
        var db = LocalStorage.openDatabaseSync("DBIssues", "","DB",1000000);

        db.transaction( function(tx)
        {
            var rs = tx.executeSql('update Issues set State=2 where ID=?', [issueID])
        } )
    }
}
