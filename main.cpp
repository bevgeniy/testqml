#include <QtGui/QGuiApplication>
#include "qtquick2applicationviewer.h"
#include <QQmlEngine>
#include <QDir>
#include <QDebug>
#include <QQmlComponent>
#include <QCryptographicHash>

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);
    QQmlEngine engine;
    engine.setOfflineStoragePath(QString(QDir::currentPath() + "/Storage"));
    qDebug()<<"Storage path is "<<engine.offlineStoragePath();
    QString new_name = QString(QCryptographicHash::hash(("DBIssues"),QCryptographicHash::Md5).toHex());
    //QString new_name = "DBIssues";
    QString fileToPath = engine.offlineStoragePath() + "/Databases/" + new_name + ".sqlite";
    QFile file(fileToPath);
    QFile fileCopyFrom(QString(QDir::currentPath() + "/DBIssues.db"));

    if(!QDir(engine.offlineStoragePath() + "/Databases").exists())
    {
        if(!QDir(engine.offlineStoragePath()).exists())
            QDir().mkdir(engine.offlineStoragePath());

        QDir().mkdir(engine.offlineStoragePath() + "/Databases");
    }

    if(!file.exists())
    {
        qDebug()<<"File "<<file.fileName()<<" no exists";

        if(fileCopyFrom.exists())
            fileCopyFrom.copy(fileToPath);

    }

    QQmlComponent component(&engine, QUrl("qml/testTask/main.qml"));
    QQuickWindow *window = qobject_cast<QQuickWindow*>(component.create());
    window->show();
    //setSource(QStringLiteral("qml/testTask/main.qml"));
    //viewer.showExpanded();
    return app.exec();
}
